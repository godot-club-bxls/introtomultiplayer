extends Node

var playerScene : PackedScene = preload("res://scenes/Player.tscn")
func create_new_player_node(id, color, myself):
	var newPlayer = playerScene.instance()
	newPlayer.name = str(id)
	newPlayer.modulate = color
	newPlayer.myself = myself
	return newPlayer
