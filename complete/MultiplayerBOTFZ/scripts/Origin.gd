extends Node2D

var SERVER_PORT = 13792
var SERVER_IP = "192.168.1.22"
var MAX_PLAYERS = 8
var peer
func _ready() -> void:
	peer = NetworkedMultiplayerENet.new()
	peer.create_client(SERVER_IP, SERVER_PORT)
	get_tree().network_peer = peer
	
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")

func _connected_ok():
	print("_connected_ok")

func _server_disconnected():
	print("_server_disconnected")

func _connected_fail():
	print("_connected_fail")

remote func new_player(id, color, myself):
	print("new_player ", id)
	var newPlayer = Utils.create_new_player_node(id, color, myself)
	$Characters.add_child(newPlayer)
	
remote func player_disconnected(id):
	print("player_disconnected ", id)
	$Characters.remove_child($Characters.get_node(id))
	
func _process(delta):
#	print(Engine.get_frames_per_second())
	pass
