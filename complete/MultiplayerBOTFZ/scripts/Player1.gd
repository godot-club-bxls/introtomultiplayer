extends KinematicBody2D

var myself = false
var direction = Vector2()
var slide = false
var punch = false

var owning:Array = []
var touching:float = 20
var close_enough:float = 80

func get_input():

	slide = false
	punch = false
#	# Detect up/down/left/right keystate and only move when pressed.
#	if Input.is_action_pressed('ui_select'):
#		slide = true
#	if Input.is_action_pressed("ui_focus_next"):
#		punch = true
#	direction = Vector2()
#	if Input.is_action_pressed('ui_right'):
#		direction.x += 1	
#	if Input.is_action_pressed('ui_left'):
#		direction.x -= 1
#	if Input.is_action_pressed('ui_down'):
#		direction.y += 1
#	if Input.is_action_pressed('ui_up'):
#		direction.y -= 1
#	direction = direction.normalized()
#
##	rpc_unreliable_id(1, "input", direction, slide, punch)
#	var dir:Vector2 = Vector2.ZERO
#	randomize()
#	var r:float = rand_range(0,3)
#	if r <= 1:
#		dir.x = 1
#	elif r <= 2:
#		dir.x = -1
#	r = rand_range(0,3)
#	if r <= 1:
#		dir.y = 1
#	elif r <= 2:
#		dir.y = -1
	
	var aiming_at:int = -1
	var closest:float = 0
	var dir:Vector2 = Vector2.ZERO
	for i in range(0,3):
		var target_p:Vector2 = get_parent().get_child(i).position
		var diff = target_p - position
		var d:float = diff.length()
		if i in owning and d < close_enough:
			continue
		elif i in owning and d >= close_enough:
			owning.erase( i )
		elif not i in owning and d < touching:
			owning.append(i)
		if aiming_at == -1 or closest > d:
			aiming_at = i
			dir = diff
			closest = d
	dir = dir.normalized()
	if not owning.empty() and randi() % 3 == 0:
		dir = Vector2.ZERO
		print( "NO MOVE!" )
	rpc_unreliable_id(1, "input", dir, slide, punch)

var linearVelocity = Vector2.ZERO
func _physics_process(delta):
	move_and_slide(linearVelocity)
	if myself:
		get_input()

remote func move(new_position, flip, animation, newLinearVelocity):
	position = new_position
	z_index = position.y
	$AnimatedSprite.flip_h = flip
	$AnimatedSprite.animation = animation
	linearVelocity = newLinearVelocity
