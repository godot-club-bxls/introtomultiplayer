extends Node2D

remote func new_tile(position, animation, withLamp):
	var newTile :AnimatedSprite = $Tile.duplicate()
	newTile.position = position
	newTile.animation = animation
	if(!withLamp):
		newTile.remove_child(newTile.get_children()[0])
	else:
		newTile.get_children()[0].z_index = newTile.position.y
	add_child(newTile)
