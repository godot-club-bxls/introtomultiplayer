extends KinematicBody2D

var maxSpeed = 190
var velocity = Vector2.ZERO
var changeVelocity = 35
var playerOwner

func reset():
	playerOwner = null
	position.x = randi()
	position.y = randi()

func _physics_process(delta: float) -> void:
	if Input.is_key_pressed(KEY_P):
		return
	if Input.is_key_pressed(KEY_R):
		reset()
	randomize()
	var obstacles = (velocity.normalized()/2+Vector2(randf()-0.5,randf()-0.5)).normalized()
	var maxAtractionElements = 4
	for body in $Vision.get_overlapping_bodies():
		if(!body.name.match("Target*") and body != playerOwner):
			obstacles -= self.position.direction_to(body.position)*2.0
		elif(maxAtractionElements > 0):
			obstacles += self.position.direction_to(body.position)/4.0
			maxAtractionElements-=1
	
	if(playerOwner):
		obstacles += self.position.direction_to(playerOwner.position)
		
	velocity += obstacles.normalized() * changeVelocity
	
	var old_pos = position
	var velocity_store = velocity
	
	velocity = move_and_slide(velocity)
	if(velocity.length() > maxSpeed):
		velocity=velocity.normalized()*maxSpeed
	
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if(collision.collider.name.match("Player*")):
			playerOwner = collision.collider

	$AnimatedSprite.flip_h = velocity.dot(Vector2.RIGHT)>0
	z_index = position.y
	
	mirror_from_edges()
	for player in Global.playersNodes:
		rpc_unreliable_id(int(player.name), "move", old_pos, $AnimatedSprite.flip_h, velocity_store)
		
var minEdgeDistance = 5
onready var viewPortSize = get_viewport().size
func mirror_from_edges():
	position.x = wrapf(position.x, minEdgeDistance, viewPortSize.x-minEdgeDistance)
	position.y = wrapf(position.y, minEdgeDistance, viewPortSize.y-minEdgeDistance)
	
