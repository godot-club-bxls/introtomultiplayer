extends Node2D

var SERVER_PORT = 13792
var MAX_PLAYERS = 8
var peer
func _ready() -> void:
	peer = NetworkedMultiplayerENet.new()
	peer.create_server(SERVER_PORT, MAX_PLAYERS)
	get_tree().network_peer = peer
	
	get_tree().connect("network_peer_connected", self, "_network_peer_connected")
	get_tree().connect("network_peer_disconnected", self, "_network_peer_disconnected")

func _network_peer_connected(id):
	print("_network_peer_connected ", id)
	var newPlayer = Utils.create_new_player_node(id)
	$Characters.add_child(newPlayer)
	Global.new_player(newPlayer)
	for player in Global.playersNodes:
		rpc_id(int(player.name), "new_player", newPlayer.name, newPlayer.modulate, int(player.name)==id)
		if(int(player.name)!=id):
			rpc_id(id, "new_player", player.name, player.modulate, false)
	$Floor.initialize_on_player(id)

func _network_peer_disconnected(id):
	print("_network_peer_disconnected ", id)
	var player = $Characters.get_node(str(id))
	if(player):
		Global.player_disconnected(player)
		$Characters.remove_child(player)
	for node in Global.playersNodes:
		rpc_id(int(node.name), "player_disconnected", player.name)
