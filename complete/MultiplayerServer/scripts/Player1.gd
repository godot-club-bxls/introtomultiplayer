extends KinematicBody2D


var speed = 0
var walkSpeed = 250
var direction = Vector2()
var slide = false
var slideDuration = 0.25
onready var slideDurationTimer = slideDuration
var slideSpeed = 600
var punch = false
var slideTimeout = 2
onready var slideTimeoutTimer = slideTimeout

var clientControlled = false
func get_input():
#	return
	# Detect up/down/left/right keystate and only move when pressed.
	if Input.is_key_pressed(KEY_R):
		reset()
		
	if slide or (!slide and Input.is_action_pressed('ui_select')):
		slide = true
		return

	if !punch and Input.is_action_pressed("ui_focus_next"):
		punch = true
		
		
	if(!clientControlled):
		direction = Vector2()

	if Input.is_action_pressed('ui_right'):
		direction.x += 1
	if Input.is_action_pressed('ui_left'):
		direction.x -= 1
	if Input.is_action_pressed('ui_down'):
		direction.y += 1
	if Input.is_action_pressed('ui_up'):
		direction.y -= 1
	direction = direction.normalized()


func reset():
	position.x = randi()
	position.y = randi()

remote func input(newDirection, newSlide, newPunch):
	clientControlled = true
	slide = newSlide
	if slide or (!slide and Input.is_action_pressed('ui_select')):
		slide = true
		return
	punch = newPunch
	if !punch and Input.is_action_pressed("ui_focus_next"):
		punch = true
	direction = newDirection
	direction = direction.normalized()
	

func _physics_process(delta):
	if Input.is_key_pressed(KEY_P):
		return
	get_input()
	if(slideTimeoutTimer > 0):
		slideTimeoutTimer -= delta
		slide = false
	
	if(slide):
		speed = walkSpeed + (slideSpeed-walkSpeed) * smoothstep(0, slideDuration, slideDuration-slideDurationTimer)
		slideDurationTimer -= delta
		$AnimatedSprite.animation = "sliding"
		if(slideDurationTimer <= 0):
			slideDurationTimer = slideDuration
			slide = false
			slideTimeoutTimer = slideTimeout
	elif(punch):
		speed = 0
		$AnimatedSprite.animation = "punching"
		punch = false
	else:
		speed = walkSpeed
		if(direction.length() > 0):
			$AnimatedSprite.animation = "running"
		else:
			$AnimatedSprite.animation = "default"
	var old_pos = position
	var collision = move_and_collide(direction * speed * delta)
	if(collision):
		if(collision.collider.name.match("Target*")):
			collision.collider.set("playerOwner", self)
			$AnimatedSprite.animation = "excited"
			
	$AnimatedSprite.flip_h = direction.dot(Vector2.RIGHT)<0
	
	
	z_index = position.y
	
	mirror_from_edges()
	
	for player in Global.playersNodes:
		rpc_unreliable_id(int(player.name), "move", old_pos, $AnimatedSprite.flip_h, $AnimatedSprite.animation, direction * speed * delta)
	
var minEdgeDistance = 5
onready var viewPortSize = get_viewport().size
func mirror_from_edges():
	position.x = wrapf(position.x, minEdgeDistance, viewPortSize.x-minEdgeDistance)
	position.y = wrapf(position.y, minEdgeDistance, viewPortSize.y-minEdgeDistance)
