extends Node2D

func _ready() -> void:
	randomize()
	var windowSize :Vector2 = get_viewport().size
	var tilesSize = $Tile.frames.get_frame("1",0).get_size()*$Tile.scale
	
	for iy in range((windowSize.y/tilesSize.y*2)+1):
		var oddIYXSpacing = iy%2*tilesSize.x/2.0
		for ix in range((windowSize.x/tilesSize.x)+1):
			var newTile :AnimatedSprite = $Tile.duplicate()
			newTile.position = Vector2(oddIYXSpacing+ix*tilesSize.x, iy*tilesSize.y/2)
			newTile.animation = str(1+(randi()%15))
			newTile.get_children()[0].visible = randi()%256 == 1
			newTile.get_children()[0].z_index = newTile.position.y
			if(!newTile.get_children()[0].visible):
				newTile.remove_child(newTile.get_children()[0])
			add_child(newTile)

func initialize_on_player(id):
	for child in get_children():
		rpc_id(id, "new_tile", child.position, child.animation, child.get_child_count()>0)
