extends Node

var playerScene : PackedScene = preload("res://scenes/Player.tscn")
func create_new_player_node(id):
	var newPlayer = playerScene.instance()
	newPlayer.name = str(id)
	var color = Color(randi())
	color.a = 1.0
	color.v = min(max(color.v, 0.4), 0.85)
	newPlayer.modulate = color
	newPlayer.reset()
	return newPlayer
