extends KinematicBody2D

var velocity = Vector2.ZERO
remote func move(new_position, flip, newVelocity):
	position = new_position
	z_index = position.y
	$AnimatedSprite.flip_h = flip
	velocity = newVelocity

func _physics_process(delta: float) -> void:
	move_and_slide(velocity)
