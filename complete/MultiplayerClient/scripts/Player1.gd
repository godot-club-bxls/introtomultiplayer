extends KinematicBody2D

var myself = false
var direction = Vector2()
var slide = false
var punch = false
func get_input():
	slide = false
	punch = false
	# Detect up/down/left/right keystate and only move when pressed.
	if Input.is_action_pressed('ui_select'):
		slide = true
	if Input.is_action_pressed("ui_focus_next"):
		punch = true
	direction = Vector2()
	if Input.is_action_pressed('ui_right'):
		direction.x += 1	
	if Input.is_action_pressed('ui_left'):
		direction.x -= 1
	if Input.is_action_pressed('ui_down'):
		direction.y += 1
	if Input.is_action_pressed('ui_up'):
		direction.y -= 1
	direction = direction.normalized()

	rpc_unreliable_id(1, "input", direction, slide, punch)

var linearVelocity = Vector2.ZERO
func _physics_process(delta):
	move_and_collide(linearVelocity)
	if(myself):
		get_input()

remote func move(new_position, flip, animation, newLinearVelocity):
	position = new_position
	z_index = position.y
	$AnimatedSprite.flip_h = flip
	$AnimatedSprite.animation = animation
	linearVelocity = newLinearVelocity
